'use strict';

/**
 * Stacks
 * 28-July-2018
 * Palindrome example
 */

const readline = require('readline'),
    letters = [],
    rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

let rword = "";

// Prompt user a word
rl.question('Enter a word: ', (word) => {
    // Push letters to the stacks
    for (let i = 0; i < word.length; i++) {
        console.log(`Pushing letter ${word[i]} to the stack...`);
        letters.push(word[i]);
    }

    // Display stacks
    console.log(`Current stacks value: ${letters}`);
    console.log('\n');

    // Pop letters off from the stacks and store into variable "rword"
    for (let i = 0; i < word.length; i++) {
        console.log(`Popping letter ${letters[letters.length - 1]} off the stack...`);
        rword += letters.pop();
    }

    // Display result
    console.log(`Retrieved word: ${rword}\n`);
    if (rword === word) {
        console.log(`The word "${word}" is a palindrome.`);
    } else {
        console.log(`The word "${word}" is not a palindrome.`);
    }

    // Close readline
    rl.close();
});