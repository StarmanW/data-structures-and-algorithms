'use strict';

/**
 * @author StarmanW
 * @version v1.0.0
 * 
 * Stacks class
 */

class Stack {
    /**
     * Constructor method 
     * 
     * @constructor
     * @param {*} data 
     * @param {*} args 
     */
    constructor(data, ...args) {
        // Initialize counter and empty stack storage
        this._count = 0;
        this.storage = {};

        if (data !== undefined) {
            this.storage[this._count++] = data;

            if (args.length) {
                args.forEach((data) => {
                    this.storage[this._count++] = data;
                });
            }
        }
    }

    /**
     * Method to push data to the stack storage
     * 
     * @param {*} data 
     */
    push(data) {
        this.storage[this._count++] = data;
    }

    /**
     * Method to pop single data from the stack
     * storage.
     * 
     * @return {*}
     */
    pop() {
        if (this._count === 0) {
            return undefined;
        }
        let result = this.storage[--this._count];
        delete this.storage[this._count];
        return result;
    }

    /**
     * Method to get current size of the stack
     * storage
     * 
     * @return {number}
     */
    size() {
        return this._count;
    }

    /**
     * Method to peek the last data in the
     * stack storage.
     * 
     * @return {*}
     */
    peek() {
        return this.storage[this._count - 1];
    }
}


/**
 * Stack demo
 */
const stack = new Stack(['a', 2, {a:2, b:4}], 'stack 2', 'stack 3', 123, {a:2});

console.log(stack.storage);
console.log(`Stack size: ${stack.size()}`);
console.log(stack.peek());

stack.push('another stack pushed!');
console.log(stack.storage);
console.log(`Stack size: ${stack.size()}`);