'use strict';

class Node {
    /**
     * Node constructor method
     * @param {*} data 
     * @param {Node} next 
     */
    constructor(data = null, next = null) {
        this.data = data;
        this.next = next;
    }
}

class LinkedList {
    /**
     * LinkedList constructor method
     */
    constructor() {
        this.head = null;
        this.length = 0;
    }

    /**
     * Add new node to the end of the list.
     * 
     * @param {*} data 
     */
    add(data) {
        // Create new node
        const newNode = new Node(data);
        
        // Check if head exist
        if (!this.head) {
            this.head = newNode;
        } else {
            // Assign head node to var
            let node = this.head;
            
            // Loop to get to the last node
            while (node.next) {
                node = node.next;
            }

            /**
             * Assign the last node 'next' to
             * referencing the new node.
             */
             node.next = newNode;
        }

        // Increment node length
        this.length++;
    }

    /**
     * Get the head node.
     * 
     * @return {Node}
     */
    head() {
        return this.head;
    }

    /**
     * Get the size of the list.
     * 
     * @return {Number}
     */
    size() {
        return this.size;
    }

    /**
     * Get boolean is empty list.
     * 
     * @return {Boolean}
     */
    isEmpty() {
        return this.length === 0;
    }
}

const ll = new LinkedList();

ll.add(1);
ll.add(2);
ll.add(3);
ll.add(4);
console.log(ll.head);
