'use strict';

/**
 * @author StarmanW
 * @version v1.0.0
 * 
 * Queue class
 */
class Queue {
    /**
     * Constructor method
     * 
     * @param {*} data 
     * @param {*} datas 
     */
    constructor(data, ...datas) {
        this._collection = [];

        if (data !== undefined) {
            this._collection.push(data);
            datas.forEach((data) => {
                this._collection.push(data);
            });
        }
    }

    /**
     * Enqueue method, push new element to
     * the queue.
     * 
     * @param {*} element
     * @return {number} 
     */
    enqueue(element) {
        return this._collection.push(element);
    }

    /**
     * Dequeue method, shift queue to remove
     * the first element in queue.
     *  
     * @return {*}
     */
    dequeue() {
        return this._collection.shift();
    }

    /**
     * Get the first element in the queue.
     * 
     * @return {*}
     */
    frontQueue() {
        return this._collection[0];
    }

    /**
     * Get the last element in the queue.
     * 
     * @return {*}
     */
    lastQueue() {
        return this._collection[this._collection.length - 1];
    }

    /**
     * Get the queue.
     * 
     * @return {Array}
     */
    getQueue() {
        return this._collection;
    }

    /**
     * Get queue length.
     * 
     * @return {number}
     */
    size() {
        return this._collection.length;
    }

    /**
     * Test if queue is empty or not.
     *  
     * @return {boolean}
     */
    isEmpty() {
        return (this._collection.length === 0);
    }
}

/**
 * Main program demo
 */
const queue = new Queue('a', 'b', 'c', 'd');
console.log(queue.enqueue('e'));
console.log(queue.enqueue('f'));
console.log(queue.getQueue());
console.log(queue.dequeue());
console.log(queue.size());
console.log(queue.isEmpty());
console.log(queue.getQueue());