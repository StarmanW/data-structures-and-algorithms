'use strict';

/**
 * @author StarmanW
 * @version v1.0.0
 * 
 * Queue class
 * 
 * Ascending order
 * 1 - Highest priority
 */
class PriorityQueue {

    /**
     * Constructor method
     * 
     * @param {Array} data 
     */
    constructor(data) {
        this._collection = [];

        // Add data to collection
        if (data !== undefined && Array.isArray(data)) {
            data.forEach((data) => {
                this.enqueue(data[0], data[1]);
            });
        }
    }

    /**
     * Enqueue method, push new element to
     * the queue.
     * 
     * @param {*} element
     * @return {number} 
     */
    enqueue(element, priority) {
        if (this.isEmpty()) {
            this._collection.push([element, priority]);
        } else {
            let added = false;

            /**
             * For loop compare priority
             */
            for (let i = 0; i < this._collection.length; i++) {
                if (priority < this._collection[i][1]) {
                    // Push the new element to before/front of the current index element
                    this._collection.splice(i, 0, [element, priority]);
                    added = true;
                    break;
                }
            }

            /**
             * If new element priority is less (higher number) than 
             * other element, push the new element to the end of
             * the collection.
             * 
             * 3 < 3 return false, thus same priority element
             * will be push to the end of that same priority
             */
            if (!added) {
                this._collection.push([element, priority]);
            }
        }
    }

    /**
     * Dequeue method, shift queue to remove
     * the first element in queue.
     * 
     * @return {*}
     */
    dequeue() {
        return this._collection.shift();
    }

    /**
     * Get the first element in the queue.
     * 
     * @return {*}
     */
    frontQueue() {
        return this._collection[0];
    }

    /**
     * Get the last element in the queue.
     * 
     * @return {*}
     */
    lastQueue() {
        return this._collection[this._collection.length - 1];
    }

    /**
     * Get the queue.
     * 
     * @return {Array}
     */
    getQueue() {
        return this._collection;
    }

    /**
     * Get queue length.
     * 
     * @return {number}
     */
    size() {
        return this._collection.length;
    }

    /**
     * Test if queue is empty or not.
     *  
     * @return {boolean}
     */
    isEmpty() {
        return (this._collection.length === 0);
    }
}

/**
 * MAIN PROGRAM DEMO
 */

const COLLECTION = [
    ['abc', 3],
    ['abc2', 2],
    ['abc3', 4],
    ['p2', 1]
];
const pq = new PriorityQueue(); // Or new PriorityQueue(COLLECTION);
pq.enqueue('abc', 3);
pq.enqueue('abc2', 2);
pq.enqueue('abc3', 4);
pq.enqueue('p2', 1);
console.log(pq.getQueue());