'use strict';

/**
 * @author StarmanW
 * @version v1.0.0
 * 
 * MySet class
 */

class MySet {
	/**
	 * Contructor method.
	 */
	constructor() {
		this._collection = [];
	}

	/**
	 * Method that check if a Set has the 
	 * element passed in as argument.
	 * 
	 * @param {*} element 
	 * @return {boolean}
	 */
	has(element) {
		return (this._collection.indexOf(element) !== -1);
	}

	/**
	 * Returns the collection
	 * 
	 * @return {array}
	 */
	values() {
		return this._collection;
	}

	/**
	 * Method to add element to the collection.
	 * 
	 * @param {*} element 
	 * @return {boolean}
	 */
	add(element) {
		if (!this.has(element)) {
			this._collection.push(element);
			return true;
		}
		return false;
	}

	/**
	 * Remove an element from the collection.
	 * 
	 * @param {*} element 
	 * @return {boolean}
	 */
	remove(element) {
		if (this.has(element)) {
			const elementIndex = this._collection.indexOf(element);
			this._collection.splice(elementIndex, 1);
			return true;
		}
		return false;
	}

	/**
	 * Return the size of the collection
	 * 
	 * @return {number}
	 */
	size() {
		return this._collection.length;
	}

	/**
	 * Union two sets and return a new Set
	 * containing the values from the two
	 * sets without duplicated values.
	 * 
	 * @static
	 * @param {MySet} firstSet
	 * @param {MySet} secondSet
	 * @return {MySet}
	 */
	static union(firstSet, secondSet) {
		const unionSet = new MySet();

		firstSet.values().forEach((e) => {
			unionSet.add(e);
		});
		secondSet.values().forEach((e) => {
			unionSet.add(e);
		});
		return unionSet;
	}

	/**
	 * Find an intersection between the two sets passed
	 * in as argument. Returns a new Set containing
	 * only the common values between the two Sets.
	 * 
	 * @static
	 * @param {MySet} firstSet
	 * @param {MySet} secondSet  
	 * @return {MySet}
	 */
	static intersection(firstSet, secondSet) {
		const intersectionSet = new MySet();
		let setA = firstSet.size() > secondSet.size() ? firstSet : secondSet,
			setB = setA === firstSet ? secondSet : firstSet;

		setA.values().forEach((e) => {
			if (setB.has(e)) {
				intersectionSet.add(e);
			}
		});
		return intersectionSet;
	}

	/**
	 * Find the difference between the two
	 * sets passed into the argument.
	 * 
	 * @static
	 * @param {MySet} firstSet
	 * @param {MySet} secondSet
	 * @return {MySet}
	 */
	static difference(firstSet, secondSet) {
		const differenceSet = new MySet();
		let setA = firstSet.size() > secondSet.size() ? firstSet : secondSet,
			setB = setA === firstSet ? secondSet : firstSet;

		setA.values().forEach((e) => {
			if (!setB.has(e)) {
				differenceSet.add(e);
			}
		});
		return differenceSet;
	}

	/**
	 * Determine if the other set passed in is
	 * a subset of the current MySet instance.
	 * 
	 * @param {MySet} otherSet 
	 */
	subset(otherSet) {
		return this.values().every((e) => {
			return otherSet.has(e);
		});
	}

	/**
	 * Determine if the current MySet instance
	 * is a subset of the other set passed in.
	 * 
	 * @param {MySet} otherSet 
	 */
	isSubset(otherSet) {
		return otherSet.values().every((e) => {
			return this.has(e);
		});
	}
}

/**
 * MAIN PROGRAM DEMO
 */
const setA = new MySet(),
	setB = new MySet();

// Add element to set A
setA.add('A');
setA.add('B');
setA.add('C');
setA.add('D');

// Add element to set b
setB.add('A');
setB.add('B');
setB.add('C');
setB.add('D');
setB.add('F');
setB.add('G');

console.log(setA.add('I'));
// 'A', 'B', 'C', 'D', 'I'
console.log(`Set A: ${setA.values()} | Size: ${setA.size()}`);
console.log(`Removed I ? ${setA.remove('I')} | Set A: ${setA.values()}\n\n`);

// 'A', 'B', 'C', 'D', 'F', 'G'
console.log(MySet.union(setB, setA));
// 'F', 'G'
console.log(MySet.difference(setB, setA));
// 'A', 'B', 'C', 'D'
console.log(MySet.intersection(setA, setB));