'use strict';

/**
 * @author StarmanW
 * @version v1.0.0
 * 
 * MyMap class
 */

class MyMap {
    /**
     * Consturctor method
     * 
     * @param {Array} data 
     */
    constructor(data) {
        this._collection = {};
        this._count = 0;

        if (data && Array.isArray(data)) {
            data.forEach((d) => this.set(d[0], d[1]));
        }
    }

    /**
     * Setter method
     * 
     * @param {*} key 
     * @param {*} value 
     */
    set(key, value) {
        this._collection[key] = value;
        this._count++;
    }

    /**
     * Getter method
     * 
     * @param {*} key 
     * @returns {*}
     */
    get(key) {
        return key in this._collection ? this._collection[key] : null;
    }

    /**
     * Has method, check if a specific
     * key exits in the collection.
     * 
     * @param {*} key 
     * @returns {Boolean}
     */
    has(key) {
        return key in this._collection;
    }

    /**
     * Delete a specific data in the map
     * based on the key passed in.
     * 
     * @param {*} key 
     */
    delete(key) {
        if (key in this._collection) {
            delete this._collection[key];
            this._count--;
        }
    }

    /**
     * Get the entries of the map.
     * 
     * @returns {Array}
     */
    entries() {
        let entries = [];
        Object.entries(this._collection).forEach((v) => entries.push(v));
        return entries;
    }

    /**
     * Get the keys in the map.
     * 
     * @returns {Array}
     */
    keys() {
        let keys = [];
        Object.keys(this._collection).forEach((k) => keys.push(k));
        return keys;
    }

    /**
     * Get the values in the map.
     * 
     * @returns {Array}
     */
    values() {
        let values = [];
        Object.values(this._collection).forEach((v) => values.push(v));
        return values;
    }

    /**
     * Clear collection and reset counter.
     * 
     */
    clear() {
        this._collection = {};
        this._count = 0;
    }

    /**
     * Get the size of the map.
     * 
     * @returns {Number}
     */
    size() {
        return this._count;
    }
}

/**
 * MAIN PROGRAM DEMO
 */
const MONTHS = [
    ['JAN', 1],
    ['FEB', 2],
    ['MAR', 3],
    ['APR', 4],
    ['MAY', 5],
    ['JUN', 6],
    ['JUL', 7],
    ['AUG', 8],
    ['SEP', 9],
    ['OCT', 10],
    ['NOV', 11],
    ['DEC', 12],
];
const map = new MyMap(MONTHS);

// map.delete('JAN');
console.log(`Map Get: ${map.get('NOV')}`);
console.log(`Size: ${map.size()}`);
console.log(`Keys: ${map.keys()}`);
console.log(`Values: ${map.values()}`);
console.log(`Entries: ${map.entries()}`);